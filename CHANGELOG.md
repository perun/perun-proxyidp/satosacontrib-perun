## [4.3.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.3.1...v4.3.2) (2024-03-01)


### Bug Fixes

* update link to eligibility data ([939a022](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/939a022803021a3738443a006b59dba68b88db8d))

## [4.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.3.0...v4.3.1) (2023-11-20)


### Bug Fixes

* convert list to string in session_started_with ([1615b3f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/1615b3fd0906be4c8d03d09c9b64832dbdaf5599))

# [4.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.2.1...v4.3.0) (2023-11-06)


### Features

* requester client ID microservices ([e971f10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/e971f10adbd34a47c8f07729a854b9344ddb24f2))

## [4.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.2.0...v4.2.1) (2023-10-19)


### Bug Fixes

* compatibility with pymongo 3 ([f612263](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/f612263ba95a6df8fde179c2aad49a700af90783))

# [4.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.1.4...v4.2.0) (2023-10-02)


### Features

* use new resource attributes for filtering groups ([2c87dd0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/2c87dd0e8f597f0140356411d1c8730f80800f19))

## [4.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.1.3...v4.1.4) (2023-09-29)


### Bug Fixes

* skip already single attributes in cardinality single ([c090d0f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/c090d0f4ec9cbef1a310c84d2751b696698af96e))

## [4.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.1.2...v4.1.3) (2023-09-25)


### Bug Fixes

* correct import of EntitlementUtils in userinfo addon ([3d48d49](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/3d48d4924a6ea5c552691cdd6267f8bb9c853ceb))

## [4.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.1.1...v4.1.2) (2023-09-22)


### Bug Fixes

* correct typo in forward authorization params ([624c0cb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/624c0cb90ea0c8c37ba6527116a607ee2363a3ba))

## [4.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.1.0...v4.1.1) (2023-09-20)


### Bug Fixes

* catch ModuleNotFoundError in introspection addon ([757efb7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/757efb7cdcd13fbf5286125d0c390a732966e775))

# [4.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v4.0.0...v4.1.0) (2023-08-24)


### Features

* addon for userinfo endpoint ([551e8b0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/551e8b04a88be3b429365b0b4cf38952e7e120b0))

# [4.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.8.0...v4.0.0) (2023-07-25)


### Bug Fixes

* auth_event_logging_microservice not working ([9f6181f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/9f6181fbd20db5efccb93517b99383598d53fb34))


### Features

* forward auth params microservice ([5853cdc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/5853cdc4903ee9e1c57142dbd98243c007a0db17))
* move is_banned microservice to idm ([0bcb80e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/0bcb80ebd92908a48192ebb4a8415b58af54002b))
* read perun_user_id_attribute from global cfg ([14ac856](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/14ac856202b209f409f7f800af2016fb5adc9fdd))
* rename attribute global_cfg_filepath ([824b040](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/824b0402f873850a72064a5f4e57ab69490f776c))
* update sqlalchemy to v2.0 in auth_event_logging microservice ([2f75dba](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/2f75dba8530ee9ac811526beaec93e0e481e24ed))
* update sqlalchemy to v2.0 in proxy_statistics microservice ([ed09388](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/ed09388d342dbbf956b157bcbd1566280a20bfbb))


### BREAKING CHANGES

* is_banned microservice is moved to idm package from general microservices package
* user_id_attr_name removed from microservice cfg and loaded instead from global cfg
* global_cfg_filepath renamed to global_cfg_path

# [3.8.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.7.3...v3.8.0) (2023-07-18)


### Features

* support for nameid_attributes map ([3797058](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/37970582226b1a8241f4d041cd1cefe9220a5909))

## [3.7.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.7.2...v3.7.3) (2023-07-18)


### Bug Fixes

* correct perun_entitlement microservice ([384b678](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/384b678f335a91a0f849805ad83e1eea2225a89f))

## [3.7.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.7.1...v3.7.2) (2023-07-15)


### Bug Fixes

* make step up addon compatible with idpy-oidc ([4b647c9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/4b647c9f08838d4bbad2c87f8b25ce10c21f0793))

## [3.7.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.7.0...v3.7.1) (2023-07-14)


### Bug Fixes

* absolute imports in seznam backend ([3f2e1e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/3f2e1e75c30db7cd035b770a9368cdb2bdab6201))
* create __init__ in addons folder ([3e942f2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/3e942f2e4f6a9f9d09f2c572f07a94592e9a13e1))

# [3.7.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.6.2...v3.7.0) (2023-07-11)


### Features

* auth switcher lite microservice ([d6461d4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/d6461d4d96a0ada3152247b36990d756c76588af))
* backend for Seznam OAuth ([8eca65e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/8eca65e130fe43e22e9b22bd8f8f86a587044323))

## [3.6.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.6.1...v3.6.2) (2023-07-10)


### Bug Fixes

* remove unused parameters of context_attributes ([a0f1e16](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/a0f1e16f9a6408bd23e5d028f462faeac9975a59))

## [3.6.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.6.0...v3.6.1) (2023-07-07)


### Bug Fixes

* show README on pypi.org ([cd330ca](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/cd330ca12b5cdfa6a2f0921c73bc86fda2cc7c5f))

# [3.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.5.0...v3.6.0) (2023-07-04)


### Features

* microservices to persist and evaluate auth parameters ([bb08184](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/bb0818413404b02f6d864426cbf7172fb3d9927b))

# [3.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.4.0...v3.5.0) (2023-06-15)


### Features

* satosa-oidcop add on for step-up info in introspection endpoint ([49efd3e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/49efd3efeb10277c64edf76b3ce59a9147069848))

# [3.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.3.0...v3.4.0) (2023-05-31)


### Bug Fixes

* module paths, perun_user test ([a5176d1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/a5176d1606126d0def081814a54d953888327a72))


### Features

* perunUserHashed microservice ([7082331](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/7082331c2c35e15087227d898b53fe980bb833b2))

# [3.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.2.0...v3.3.0) (2023-05-26)


### Features

* compute eligibility microservice ([c499ec9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/c499ec9df36c94bca49ca0e816f1352b0f9c050f))

# [3.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.1.0...v3.2.0) (2023-05-24)


### Features

* eligibility function for einfra ([f11ad18](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/f11ad185b9dedcebc40880ba96a2a82e2d9cdbce))

# [3.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v3.0.0...v3.1.0) (2023-05-24)


### Features

* microservice redirecting banned users ([32cd827](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/32cd827c7e834422065d2602dfb33939b5258e42))

# [3.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v2.0.0...v3.0.0) (2023-05-10)


### Features

* replace metadata reading in context_attributes_microservice ([1850926](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/18509266fd1405a30b1a9b519e304d31664be290))


### BREAKING CHANGES

* Removed description from target backend attribute in internal data,
the MetaInfoIssuer microservice needs to be run beforehand.
Patches for MetaInfoIssuer from kofzera:swamid-satosa and for
satosa from kofzera:SATOSA are needed until changes are incorporated to upstream.

# [2.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.4.0...v2.0.0) (2023-04-17)


### Bug Fixes

* add default rule for requester ([8d19792](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/8d1979252b73e28b425895c82cb37fc5621557ba))
* use default rule when target_id not set ([7caf6fc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/7caf6fc2cd4cfc9d7605e0dd3e830f380ad34c33))


### chore

* delete perun identity beta microservice ([5665938](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/56659389e8fe9752c8de7523f74747392829280a))


### Features

* allowed requesters in global config ([9e15225](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/9e15225926372fd058b63809d2e25136d8c49e41))
* move perun microservices to subpackage idm ([05a745c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/05a745c9d750034c7eb524494b730270ac30bc62))


### BREAKING CHANGES

* all perun microservices must be allowed for a
combination of requester/target entity to run
closes PRX-168
* PerunIdentityBeta microservice removed
* microservices using perun moved to idm subpackage

# [1.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.3.0...v1.4.0) (2023-03-10)


### Features

* replace pycurl with requests ([c14c718](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/c14c71883e0bbe76e715dc8778194d9c62dd907f))

# [1.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.2.1...v1.3.0) (2023-01-13)


### Features

* AuthEventLogging microservice, fix proxystatistics ([68c39b0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/68c39b06242a7e63ad1dce48b7d61237488fa388))

## [1.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.2.0...v1.2.1) (2023-01-06)


### Bug Fixes

* use perun.connector >=3.3 ([2d97279](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/2d97279fd785501aaa1c8e53f49391f486ad115d))

# [1.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.1.0...v1.2.0) (2022-11-28)


### Features

* is_eligible_microservice ([d31c9ce](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/d31c9ce86c43300b2d6a302f88de3e7085257d2d))

# [1.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.0.4...v1.1.0) (2022-10-13)


### Features

* new microservice AttributeTyping ([322a7b8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/322a7b8fd46348249aec382265756c6ddf0e516f))

## [1.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.0.3...v1.0.4) (2022-10-13)


### Bug Fixes

* only add plus sign to driver once ([2e52f2f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/2e52f2f30d0f7f48272a7e81dd70ce950267296c))
* only add plus sign to driver once ([71eea04](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/71eea04903c44b877959e95a70246e467b6ce709))

## [1.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.0.2...v1.0.3) (2022-10-13)


### Bug Fixes

* move pycurl to extras ([ab6d9f4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/ab6d9f4d5c1e409e1df34e3f40008917a76740e3))

## [1.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.0.1...v1.0.2) (2022-10-13)


### Bug Fixes

* update README.md ([bc12962](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/bc129621aa3cd27343512399e8e5da6eddf4240d))

## [1.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/compare/v1.0.0...v1.0.1) (2022-09-28)


### Bug Fixes

* correct order of semantic release plugins ([211e95d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/211e95d09e1eafabf3a29cbe42e8fa9a2ccf567f))

# 1.0.0 (2022-09-28)


### Bug Fixes

* configStore loading configs bug ([33883b9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/33883b967ab378acca5b67df15e605827244733e))
* **deps:** add perun.connector to requirements.txt SATOSA 8.0.1 => 8.1.0 ([cbcc4cd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/cbcc4cd1f0382a234f516a63bd0fe62ab8f17e86))
* **deps:** perun.connector version to 1.0.2 ([1018ac8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/1018ac8b4a5e65e4c49a193def9ec9f38c950f21))
* exceptions ([5bc9b6a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/5bc9b6a04ebe34666fb508cf43b27b22a71cba10))
* minor errors ([3c56f17](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/3c56f179e0b6a581bb79f3cbdd597d7a1377c255))


### Features

* adding unit tests ([21781b6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/21781b6759a7fb5cc715a81ae03e83eeee77986a))
* build workflow, files format with black ([e6db5db](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/e6db5db8453c93beac630ead36972192b6c30275))
* ConfigStore class to read configs from files and store them in variables ([e4c8a25](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/e4c8a2548b970f04225c541459a5c50859867ade))
* curlConnector moved to utils from perun.connector ([d21142a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/d21142a8245133f68c9e1f15736924cbf059d445))
* fist microservices ([accd91d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/accd91def5f11a4fdcba9de383b0b82ba62edf6a))
* jwt_signing_util ([54926c4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/54926c4899bb9d207cd87dd09e02132ef9fc30c4))
* microservices_global config template ([c774de9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/c774de98c8904e603da15da37e2101d68896898d))
* missing configs ([919d6c0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/919d6c09a502261a15453c6fb33cbc348a6ac7ba))
* perun_attributes_microservice and tests ([38010fc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/38010fc55a1f9e65eec25abd6acf124611f23dc7))
* perun_entitlement ([3e06f9a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/3e06f9ade55ad898963185c2c1a17841a15bdc65))
* perun_user_microservice ([9d3d691](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/9d3d691dcf3a02be51b7781fa85d2ddda68465e0))
* PerunEnsureMember ([f15e17d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/f15e17dd2db30bf882f9a977b39f599a4ba17711))
* satosacontrib.perun packages, proxystatistics_microserivce, correct module paths in microservices configs ([fb4c9b9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/fb4c9b9af28773df03fc704c5f96c74a01d78e0a))
* sp_authorization_microservice ([bc5c8b3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/bc5c8b3f349b844ea080602c1510cb7ab21b0d75))
* test_microservice_loader ([422d702](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/422d7021bb7e401ca1c1397b0fffcea69e1e9e05))
* tests global_config to tests_loader, perun_user tests reworked to tests_loader ([89a477a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/89a477aa2eb7d0bfd30ed6150506cc594e281c21))
* update_user_ext_source ([fc697b3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/satosacontrib-perun/commit/fc697b3e43c0953e0ccc52e5893e0c5360f0edf6))
