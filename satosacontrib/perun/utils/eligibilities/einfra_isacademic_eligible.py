import os
import requests
import yaml
import time

from perun.connector.utils.Logger import Logger
from satosa.internal import InternalData
from datetime import datetime, timedelta

IDP_ELIGIBLE_AFFILIATIONS_LIST_LINK = (
    "https://gitlab.ics.muni.cz/perun/deployment/e-infracz_idps_data/-/raw/main"
    "/einfra_isacademic/result.yaml"
)
LINK_REFRESH_RATE_MINS = 15  # older file will be refreshed
FILE_MAX_AGE_HOURS = 24  # older file won't be considered valid
DATA_FILEPATH = (
    "/tmp/einfra_isacademic_eligible.yml"  # where file will be stored and loaded from
)

logger = Logger.get_logger(__name__)

eligibility_data = {}


def is_eligible(data: InternalData, *args, **kwargs):
    """
    Returns authentication timestamp, if user's affiliation comply with eligible
    affiliations specified in the fetched external list of IdP: affiliations.
    Otherwise returns false.
    """
    idp = data.auth_info["issuer"]
    user_affiliations = data.attributes.get("externalaffiliation")
    if user_affiliations is None:
        return False
    eligibilities = get_eligibility_data()
    if eligibilities is None:
        return False
    eligibility_affiliations = eligibilities.get(idp)
    user_affiliations = list(aff.split("@")[0] for aff in user_affiliations)
    if eligibility_affiliations is None:
        return False
    else:
        if any(
            affiliation in eligibility_affiliations for affiliation in user_affiliations
        ):
            return time.time()
    return False


def get_eligibility_data():
    """
    Returns parsed IdP: affiliations file. Fetches data anew if file is missing
    or is older than specified refresh age. If cannot fetch new file, logs error
    and works with saved file if not older than specified age.
    """
    data = None
    if os.path.exists(DATA_FILEPATH):
        file_age = datetime.now() - datetime.fromtimestamp(
            os.path.getmtime(DATA_FILEPATH)
        )
        if file_age > timedelta(minutes=LINK_REFRESH_RATE_MINS):
            data = fetch_affiliations_data()
        if data is None:
            if file_age < timedelta(hours=FILE_MAX_AGE_HOURS):
                with open(DATA_FILEPATH, "r") as file:
                    data = yaml.safe_load(file)
    else:
        data = fetch_affiliations_data()

    return data


def fetch_affiliations_data():
    try:
        response = requests.get(IDP_ELIGIBLE_AFFILIATIONS_LIST_LINK)
        response.raise_for_status()
        with open(DATA_FILEPATH, "w") as f:
            f.write(response.text)
        return yaml.safe_load(response.text)
    except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as exc:
        logger.error(f"Fetching data for einfra-isacademic eligibility failed: {exc}")
    except yaml.YAMLError:
        logger.error("Invalid yaml format for einfra-isacademic eligibility file.")
    return None
